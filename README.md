# vue-recipes

> Learn Recipe book web

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

vue nuxt from tutorial

>> [Nuxt.js Introduction by Project](https://www.youtube.com/watch?v=nteDXuqBfn0)